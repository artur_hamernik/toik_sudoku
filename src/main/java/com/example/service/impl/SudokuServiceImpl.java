package com.example.service.impl;

import com.example.dto.ErrorDto;
import com.example.dto.SudokuDto;
import com.example.repository.SudokuRepository;
import com.example.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SudokuServiceImpl implements SudokuService {

    @Autowired
    SudokuRepository sudokuRepository;
    private ErrorDto errorDto;

    public SudokuServiceImpl() {
    }

    @Override
    public ErrorDto getErrorDto() {
        return errorDto;
    }
    @Override
    public SudokuDto getBoard(){
        return sudokuRepository.getBoard();
    }

    @Override
    public boolean verifySudoku(){
        sudokuRepository.readCsv();
        List<Integer> lineIds = new ArrayList<>();
        List<Integer> columnIds = new ArrayList<>();
        List<Integer> areaIds = new ArrayList<>();
        // row checker
        for(int row = 0; row < 9; row++)
            for(int col = 0; col < 8; col++)
                for(int col2 = col + 1; col2 < 9; col2++)
                    if(sudokuRepository.getBoard().getBoard().get(row).get(col).equals(sudokuRepository.getBoard().getBoard().get(row).get(col2))) {
                        lineIds.add(row+1);
                    }

        // column checker
        for(int col = 0; col < 9; col++)
            for(int row = 0; row < 8; row++)
                for(int row2 = row + 1; row2 < 9; row2++)
                    if(sudokuRepository.getBoard().getBoard().get(row).get(col).equals(sudokuRepository.getBoard().getBoard().get(row2).get(col)))
                        columnIds.add(col+1);

        // grid checker
        for(int row = 0; row < 9; row += 3)
            for(int col = 0; col < 9; col += 3)
                // row, col is start of the 3 by 3 grid
                for(int pos = 0; pos < 8; pos++)
                    for(int pos2 = pos + 1; pos2 < 9; pos2++)
                        if(sudokuRepository.getBoard().getBoard().get(row + pos%3).get(col + pos/3).equals(sudokuRepository.getBoard().getBoard().get(row + pos2%3).get(col + pos2/3))) {
                            switch (row){
                                case 0:
                                    switch(col){
                                        case 0:
                                            areaIds.add(1);
                                            break;
                                        case 3:
                                            areaIds.add(2);
                                            break;
                                        case 6:
                                            areaIds.add(3);
                                            break;
                                    }
                                    break;
                                case 3:
                                    switch(col){
                                        case 0:
                                            areaIds.add(4);
                                            break;
                                        case 3:
                                            areaIds.add(5);
                                            break;
                                        case 6:
                                            areaIds.add(6);
                                            break;
                                    }
                                    break;
                                case 6:
                                    switch(col){
                                    case 0:
                                        areaIds.add(7);
                                        break;
                                    case 3:
                                        areaIds.add(8);
                                        break;
                                    case 6:
                                        areaIds.add(9);
                                        break;
                                }

                                    break;
                            }
                        }

        if(!lineIds.isEmpty() || !columnIds.isEmpty() || !areaIds.isEmpty()){
            errorDto = new ErrorDto(lineIds,columnIds,areaIds);
            return false;
        }
        return true;
    }
}
