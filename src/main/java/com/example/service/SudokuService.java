package com.example.service;

import com.example.dto.ErrorDto;
import com.example.dto.SudokuDto;

public interface SudokuService {
    SudokuDto getBoard();
    boolean verifySudoku();
    ErrorDto getErrorDto();
}
