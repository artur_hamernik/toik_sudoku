package com.example.dto;

import java.util.List;

public class SudokuDto {
    private List<List<Integer>> board;

    public SudokuDto() {
    }

    public SudokuDto(List<List<Integer>> board) {
        this.board = board;
    }

    public List<List<Integer>> getBoard() {
        return board;
    }

    public void setBoard(List<List<Integer>> board) {
        this.board = board;
    }
}
