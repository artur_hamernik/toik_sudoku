package com.example.repository;

import com.example.dto.SudokuDto;

public interface SudokuRepository {
    void readCsv();
    SudokuDto getBoard();
}
