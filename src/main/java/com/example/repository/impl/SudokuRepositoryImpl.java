package com.example.repository.impl;

import com.example.dto.SudokuDto;
import com.example.repository.SudokuRepository;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.stereotype.Repository;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class SudokuRepositoryImpl implements SudokuRepository {
    private static final String FILEPATH = "sudoku.csv";
    private SudokuDto board;


    public SudokuRepositoryImpl() {
        readCsv();
    }

    @Override
    public void readCsv(){
        List<List<String>> records = new ArrayList<>();
        try (CSVReader csvReader = new CSVReader(new FileReader(FILEPATH))) {
            String[] values = null;
            while ((values = csvReader.readNext()) != null) {
                records.add(Arrays.asList(values));
            }
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        convertToIntegers(records);
    }
    @Override
    public SudokuDto getBoard(){
        return board;
    }

    private void convertToIntegers( List<List<String>> records ){
        List<List<Integer>> boardHolder;
        boardHolder = new ArrayList<>();
        for (List<String> list:records){
            List<Integer> holder = new ArrayList<>();
            for (String item:list) {
                holder.add(Integer.parseInt(item));
            }
            boardHolder.add(holder);
        }
        board = new SudokuDto(boardHolder);
    }
}
