package com.example.rest;

import com.example.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/sudoku")
public class RestController {

    @Autowired
    SudokuService sudokuService;

    @GetMapping(value = "/board")
    public ResponseEntity getBoard(){
        return ResponseEntity.ok().body(sudokuService.getBoard());
    }
    @PostMapping(value = "verify")
    public ResponseEntity verify(){
        if(sudokuService.verifySudoku()){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().body(sudokuService.getErrorDto());

    }
}
